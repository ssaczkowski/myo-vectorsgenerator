#Dimensión
param n := read "input.txt" as "1n" use 1;
param template := read "input.txt" as "1s" skip 1 use 1;
 
 
#Conjunt S
set S :=  {read "input.txt" as template skip 2};
#Indice del conjunto S
set IS := {0..card(S)-1};
 
##### AUXILIARES #####
 
#Indice para recorrer vectores
set IB := {0..2*card(S)-1};
 
#Indice para recorrer componentes
set N := {1..n};
 
#Indice para recorrer vector y componente
set IBN := {<i,k> in IB*N};
 
#Indice para recorrer vector y vector
set IBIB := {<i,j> in IB*IB};
 
#Indice para recorrer vector, vector y componente
set IBIBN := {<i,j,k> in IB*IB*N};
 
#### VARIABLES ####
var x[IB] binary;
var y[IBN] binary;
var d[IBIB] binary;
var t[IBIBN] binary;
 
#### OBJETIVO ####
minimize vectores: sum <i> in IB : x[i];
 
#### RESTRICCIONES ####
 
subto promedio: forall <i> in IS do
  forall <k> in N:
  y[2*i,k] + y[2*i+1,k] == 2 * ord(S,i+1,k);
 
subto diferencia: forall <l> in IB do
  forall <m> in IB with m < l do
  forall <k> in N:
  d[l,m] >= t[l,m,k];
 
subto difVecModPos: forall <l> in  IB do
   forall <m> in IB do
   forall <k> in N:
       t[l,m,k] >= y[l,k] - y[m,k];
 
subto difVecModNeg: forall <l> in  IB do
   forall <m> in IB do
   forall <k> in N:
       -t[l,m,k] <= y[l,k] - y[m,k];
 
subto activarX: forall <l> in IB do
  x[l] >= (sum <m> in {0..l}: d[l,m]) - (l-1);