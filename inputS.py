## SCRIPT GENERADOR DE INPUT DE S

import os
import random


def pedirNumeroEntero(pedidoString):
	correcto= False
	while(not correcto):
		try:
			num= int(input(pedidoString))
			correcto=True
		except ValueError:
			print('Error, debe de introducir un numero entero')
	return num

def generarTemplate(n):
	template='"<'
	for i in range(n):
		valor=i+1
		template=template+str(valor)+"n"
		if(i<n-1):
			template= template+","
	template= template + '>"'
	return template

def generar_inputS_txt(n,cardinalS):
	file= open("input.txt","w")
	vector=str(n)
	file.write(vector+os.linesep)
	vector=generarTemplate(n)
	file.write(vector+os.linesep)
	for i in range(cardinalS):
		vector=""
		for j in range(n):
			vector=vector+ random.choice(("0","0.5","1"))
			if(j<n-1):
				vector= vector+" "
		print(vector)
		file.write(vector+os.linesep)
	file.close()


pedir_N="Introduce un numero entero para n: "
pedir_cardinalS="Introduce un numero entero para cardinalS: "

n=pedirNumeroEntero(pedir_N)
print('N= ', n)

cardinalS=pedirNumeroEntero(pedir_cardinalS)
print('cardinalS= ', cardinalS)

generar_inputS_txt(n,cardinalS)
