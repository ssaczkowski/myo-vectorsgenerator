##### CONJUNTOS #####
param nParam := read "input.txt" as "1n" use 1; 
set N := {1..nParam};
param template := read "input.txt" as "1s" skip 1 use 1;

set S :=  {read "input.txt" as template skip 2};
set IndexS := {1..card(S)};
set B := { read "inputB_completo.txt" as template};
set IndexB := {1..card(B)};

#### VARIABLES #### 
set IndexSIndexB := {<i,j> in IndexS*IndexB};
var y[IndexSIndexB] binary;

var x[IndexB] binary;

#### OBJETIVO ####
minimize vectores: sum <v> in IndexB : x[v];

#### RESTRICCIONES ####
subto promedio: 
    forall <s> in IndexS:
        forall <n> in N:
            (sum <v> in IndexB: y[s,v] * ord(B,v,n)) == 2 * ord(S,s,n);

subto son2vectoresY: 
    forall <s> in IndexS:
        (sum <v> in IndexB: y[s,v]) == 2; 

subto XmayorIgualY: 
    forall <s> in IndexS:
        forall <v> in IndexB:
            x[v] >= y[s,v];
